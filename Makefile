ifndef CFLAGS
CFLAGS = -O2 -g -std=c++11  -I ../linux/bpir1-hdr/include/
endif
CC=g++
all: nmap-parse

%.o: %.cpp
	$(CC) $(CFLAGS) -c -o $@ $^

nmap-parse: nmap-parse.o 
	$(CC) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	rm -f *~ *.o nmap-parse

install:
	cp nmap-parse /etc/bin
