#include <vector>
#include <iostream>
#include <fstream>
#include <cstring>
#include <regex>


using namespace std;

// parse the output of the command 'nmap -sn ipmask' and extract IPs and MACs"


int main( const int argc, const char *argv[] )
{
   if ( argc != 2 ) {
      cout << "parse the output of the command 'nmap -sn lanipmask' and extract IPs and MACs";
      cout << "syntax: nmap-parse xmlfilename\n";
      cout << "example: nmap-parse log.xml\n";
      return 1;
   }


   typedef pair<string,string> pair_t;
   typedef vector<pair_t> vec_t;
   vec_t addr_list;

   regex mac_reg("[[:xdigit:]:]{17}");
   regex ip_reg("[0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}");

   string host_begin("<host>");
   string host_end("</host>");

   smatch ip,mac;
   string line;

   size_t index = 0;
   ifstream ifile( argv[1] );
   while(getline(ifile,line)) {
      if ( line.find( host_begin ) != string::npos )
         addr_list.push_back( pair_t() );
      else if ( line.find( host_end ) != string::npos )
         ++index;
      else if ( addr_list.size() == index+1 ) {
         if ( regex_search( line, ip, ip_reg ) )
            addr_list[index].first = ip.str();
         if (regex_search( line, mac, mac_reg ) )
            addr_list[index].second = mac.str();
      }
   }  // while loop

   for ( vec_t::const_iterator i=addr_list.begin(); i != addr_list.end(); ++i )
      cout << i->first << ";" << i->second << endl;
}
